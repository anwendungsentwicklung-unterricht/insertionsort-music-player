package com.company;

import java.util.ArrayList;
import java.util.Collections;

public class Album {
    private ArrayList<Title> list = new ArrayList<Title>();

    public void addTitle(String title, String singer, int price) {
        Title item = new Title(title, singer, price);
        list.add(item);
    }

    public ArrayList<Title> getList() {
        return this.list;
    }

    public void setList(ArrayList<Title> arrayList) {
        this.list = arrayList;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Title title : list) {
            result.append(", ").append(((Title) title).getTitle());
        }

        return result.toString();
    }


    public static ArrayList<Title> InsertionSortEasy(ArrayList<Title> arrayList) {
        for (int i = 1; i < arrayList.size(); i++) {
            Title title = arrayList.get(i);
            int length = title.getTitle().length();

            int j = i;

            while (j > 0 && arrayList.get(j - 1).getTitle().length() > length) {
                arrayList.set(j, arrayList.get(j - 1));
                j--;
            }

            arrayList.set(j, title);
        }
        return arrayList;
    }

    public static ArrayList<Title> InsertionSort(ArrayList<Title> arrayList, boolean reverse) {
        for (int i = 1; i < arrayList.size(); i++) {
            Title title = arrayList.get(i);
            int length = title.getTitle().length();

            int j = i;

            while (j > 0 && arrayList.get(j - 1).getTitle().length() > length) {
                arrayList.set(j, arrayList.get(j - 1));
                j--;
            }

            arrayList.set(j, title);
        }
        if (reverse) {
            Collections.reverse(arrayList);
        }

        return arrayList;
    }

    public static ArrayList<Title> InsertionSortByTitle(ArrayList<Title> arrayList, boolean reverse) {

        arrayList.sort(new TitleComparator());
        if (reverse) {
            Collections.reverse(arrayList);
        }
        return arrayList;
    }
}
