package com.company;

import java.util.Comparator;

public class Title implements Comparator<Title> {
    private String title;
    private String singer;
    private int songLength;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public int getSongLength() {
        return songLength;
    }

    public void setSongLength(int songLength) {
        this.songLength = songLength;
    }

    Title(String title, String singer, int songLength) {
        this.title = title;
        this.singer = singer;
        this.songLength = songLength;
    }


    @Override
    public int compare(Title o1, Title o2) {
        // TODO Auto-generated method stub
        return o2.getTitle().compareTo(o1.getTitle());
    }
}
