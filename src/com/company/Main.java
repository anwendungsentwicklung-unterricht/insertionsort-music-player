package com.company;

public class Main {

    public static void main(String[] args) {

        int[] unsorted = {4, 1, 8, -3, 5, 7};
        int[] sort = insertionSort(unsorted);

        for (int j : sort) {
            System.out.print(j + ", ");
        }

    }

    public static int[] insertionSort(int[] sortBy) {
        for (int i = 1; i < sortBy.length; i++) {
            int temp = sortBy[i];
            int j = i;
            while (j > 0 && sortBy[j - 1] > temp) {
                sortBy[j] = sortBy[j - 1];
                j--;
            }
            sortBy[j] = temp;
        }
        return sortBy;
    }
}
