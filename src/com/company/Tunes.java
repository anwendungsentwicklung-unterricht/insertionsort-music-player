package com.company;

public class Tunes {
    public static void main(String[] args) {
        Album music = new Album();

        music.addTitle("By the Way", "Red Hot Chili Peppers", 6);
        music.addTitle("Come On Over", "Shania Twain", 5);
        music.addTitle("Soundtrack", "The Producers", 4);
        music.addTitle("Play", "Jennifer Lopez", 3);

        music.addTitle("Double Live", "Garth Brooks", 2);
        music.addTitle("Greatest Hits", "Stone Temple Pilots", 1);

        System.out.println("Original:");
        System.out.println(music);


        // 1. Implementieren Sie den Insertionsort
        // https://moodle2.flbk-hamm.de/mod/assign/view.php?id=567
        System.out.println("1. Implementieren Sie den Insertionsort:");
        music.setList(Album.InsertionSortEasy(music.getList()));
        System.out.println(music);

        // 2. Implementieren Sie einen Sortieralgorithmus mit neuer Sortierreihenfolge
        // https://moodle2.flbk-hamm.de/mod/assign/view.php?id=568
        System.out.println("2. Implementieren Sie einen Sortieralgorithmus mit neuer Sortierreihenfolge");
        music.setList(Album.InsertionSort(music.getList(), true));
        System.out.println(music);

        // 3. Implementieren Sie einen Sortieralgorithmus nach Titel
        // https://moodle2.flbk-hamm.de/mod/assign/view.php?id=569
        System.out.println("3. Implementieren Sie einen Sortieralgorithmus nach Titel");
        music.setList(Album.InsertionSortByTitle(music.getList(), false));
        System.out.println(music);
    }
}
