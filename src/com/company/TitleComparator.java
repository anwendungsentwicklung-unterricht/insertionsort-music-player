package com.company;

import java.util.Comparator;

public class TitleComparator implements Comparator<Title> {
    @Override
    public int compare(Title o1, Title o2) {

        return o1.getTitle().compareTo(o2.getTitle());
    }


}
